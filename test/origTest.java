import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class origTest {
    @org.junit.Test
    public void testCorrect() {
        assertEquals(1, orig.find(60));
    }

    @Test(expected = ArithmeticException.class)
    public void test10^9() {
        orig.scanTest(1000000001);
    }
}