import java.util.InputMismatchException;
import java.util.Scanner;

public class orig {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        try { //проверка неправильного ввода
            n = sc.nextInt();
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.exit(0);
        }
        if (n == 0) {//если переменная 0
            System.out.println(1);
        } else {
            scanTest(n);//проверка на превышение 10^9
            int count = find(n);
            if (count == 0) {
                System.out.println("нет");
            } else {
                System.out.println("Кратно: " + count);
            }
        }
    }


    public static void scanTest(int n) throws ArithmeticException {
        if (n > Math.pow(10, 9) || n < 0) {
            throw new ArithmeticException("Должно быть положительным и меньше 10^9");
        }
    }

    public static int find(int n) {
        int count = 0;//int count = 1
        while (n > 0) {
            int digit = n % 10;
            System.out.println("проверяется " + digit);
            if (digit % 5 == 0) {
                System.out.println(digit + " кратно 5");
                count++;//count = count + digit
            }
            n = n / 10;
        }
        return count;
    }
}
